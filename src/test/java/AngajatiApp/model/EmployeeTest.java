package AngajatiApp.model;
import AngajatiApp.controller.DidacticFunction;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTimeout;
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)

class EmployeeTest {
    private Employee e1;



    @BeforeEach
    void setUp() throws Exception {
        e1=new Employee();
        e1.setId(Integer.parseInt("1"));
        e1.setFirstName("Sergiu");
        e1.setLastName("Casuneanu");
        e1.setFunction(DidacticFunction.valueOf("TEACHER"));
        e1.setCnp("123456");
        e1.setSalary(Double.valueOf("4000"));
        System.out.println("Setup employee");
    }

    @AfterEach
    void tearDown(){
        System.out.println("tearDown");
    }

    @Test
    @Order(8)
    void getFirstName() {
        assertEquals("Sergiu", e1.getFirstName(),"Numele nu este Sergiu");
        System.out.println("Numele este ok");
    }

    @Test
    @Order(2)
    void setFirstName() {
        e1.setFirstName("Cristian");
        assertEquals("Cristian",e1.getFirstName(), "Numele nu este Cristian");
        System.out.println("Numele este ok");
    }

    @Test
    @Order(5)
    void getFunction() {
        assertEquals(DidacticFunction.valueOf("TEACHER"), e1.getFunction(), "Functia nu este TEACHER");
        System.out.println("Functia este ok");
    }

    @Test
    @Order(6)
    void setFunction() {
        e1.setFunction(DidacticFunction.valueOf("CONFERENTIAR"));
        assertEquals(DidacticFunction.valueOf("CONFERENTIAR"), e1.getFunction(), "Functia nu este CONFERENTIAR");
        System.out.println("Functia este ok");
    }

    @Test
    @Order(3)
    void getSalary() {
        assertEquals(Double.valueOf("4000"), e1.getSalary(),"Salariul nu este 4000");
        System.out.println("Salariul este ok");
    }

    @Test
    @Order(4)
    void setSalary() {
        e1.setSalary(Double.valueOf("4400"));
        assertEquals(Double.valueOf("4400"), e1.getSalary(),"Salariul nu este 4400");
        System.out.println("Salariul este ok");

    }

    @Test
    @Order(1)
    void testConstructor(){
        Employee e1 = new Employee();
        assertEquals("", e1.getFirstName(),"Numele nu ar trebui sa fie completat");
        System.out.println("Constructorul este ok");
    }

    @ParameterizedTest
    @ValueSource(strings={"vald","andreea","calin","andrei"})
    void testParametrizatSetareNume(String nume){
        e1.setFirstName(nume);
        assertEquals(nume, e1.getFirstName(),"numele nu este"+nume);
        System.out.println("setare Nume "+nume+" este ok");
    }


    @Disabled
    @Test
    @Timeout(1)
    void unbracable(){
        int i = 0;
        while(i==0){
            System.out.println("0101000010101111101");
        }
    }
    @Test
    void timeoutexceed(){
        assertTimeout(Duration.ofMillis(200),() -> {
            Thread.sleep(100);
        });
    }
}